
/**
 * Inside of a class Recursion, and using recursion, write a method recursiveCount() that accepts as input a String[] words and an int n. 
 * The method should return the number of Strings in words that:

�         Contain the character q in them. (Hint: Remember, there is a library method to check this.)

�         Are located within words at an even index (e.g. 0,2,4,6, etc) that is greater than or equal to n.


 * @author Maria Barba 1932657
 *
 */
public class Recursion {
	public static void main(String [] args) {
		String[] testWords = {"qizza", "queen", "qlown", "quote","q"};
		Recursion recObj=new Recursion(); 
		System.out.println("The number of mathes " +recObj.recursiveCount(testWords,1));
	}
	
	public int recursiveCount(String[] initialWords, int n) {
		if(n>=initialWords.length) {
		return 0;
		}
		int count=recursiveCount(initialWords,n+1);
		//System.out.println("This is count "+count);
		//System.out.println("This is n "+n);
		return initialWords[n].contains("q") && n%2==0 ? count+1 : count;	
		}
		
		
	}
	



