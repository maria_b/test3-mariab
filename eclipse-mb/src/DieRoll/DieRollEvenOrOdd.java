package DieRoll;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
/**
 * 
 * @author Maria 
 *
 */
public class DieRollEvenOrOdd implements EventHandler<ActionEvent> {
	private TextField message;
	private TextField result;
	private String playerChoice;
	private DieRollGame myGame;
	//welcome, wins, losses, userMoney, "Even", theGame)
	public DieRollEvenOrOdd(TextField message,TextField result,String playerChoice,DieRollGame myGame) {
		this.message=message;
		this.playerChoice=playerChoice;
		this.myGame=myGame;
		this.result=result;
	}
	@Override
	public void handle(ActionEvent e) {
		
		int userMoneyAmount=myGame.getUserMoney();
		String response= "" +myGame.playRound(playerChoice,userMoneyAmount);
		result.setText(response);
		
	}

}

