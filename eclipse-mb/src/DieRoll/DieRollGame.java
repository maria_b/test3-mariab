package DieRoll;

import java.util.Random;
import java.util.Scanner;

/*Using JavaFX, design and implement a program that allows the user to place bets on whether a die roll
will show an even (2,4,6) or an odd (1,3,5) number of dots.
Note: This question does NOT involve networking in any way. That is, you can and should code the whole task as a single program.

Your program should do the following:

�         The user always starts out with 250 units of money

�         The user can enter, via a TextField, the amount of money they wish to bet each round.

�         In each round, the user chooses whether to bet on even or odd by hitting a button for even or a button for odd.
         This button uses the above text field to get the amount bet in the round.

�         If the user has placed an invalid bet (e.g. they don�t have enough money, they didn�t enter a number, etc) then there 
          should be no change. A message should be displayed to the user indicating the issue.

�         If the user has placed a valid bet, you should simulate a roll of a die, which has a 50% chance of being even and a 50% 
          chance of being odd, using a random number. If the user is correct, they gain the money they bet. 
          If they are wrong they lose the money they bet.

�         The program must display what happened in each round as well as how much money the user has. 
          You may choose how you�d like to display this information.
*/
/**
 * This class will contain all the backend code that relates to the DieRoll game
 * @author Maria Barba 1932657
 *
 */
public class DieRollGame {
	private int numWins ;
	private int numLosses ;
	private int userMoney;
	private Random rand;
	
	public DieRollGame() {
		this.numWins=0;
		this.userMoney=250;
		this.numLosses=0;
		this.rand=new Random();;
	}
	/**
	 * @return the userMoney
	 */
	public int getUserMoney() {
		return userMoney;
	}
	/**
	 * @param userMoney the userMoney to set
	 */
	public void setUserMoney(int userMoney) {
		this.userMoney = userMoney;
	}
	public int getNumWins() {
		return this.numWins;
	}

	public int getNumLosses() {
		return this.numLosses;
	}
	public static void main(String[] args) {
		DieRollGame game1=new DieRollGame ();
		//Generate game object
		Scanner myInput = new Scanner(System.in);
		//Generate scanner obj
		System.out.println("Hello user , how much of your money are you willing to bet today ? ");
		int userMoneyInput = 0;
		userMoneyInput=myInput.nextInt();
		
		while(!game1.checkValidMoney(userMoneyInput)) {
		System.out.println("Invalid input , try inputting again ");
		userMoneyInput = myInput.nextInt();
		}
		System.out.println("Do you think the die will roll even or odd ? :");
		System.out.println("0-Even , 1-Odd");
		
		int playercChoice = myInput.nextInt();
		String playerChosen = null;
		switch (playercChoice) {
		case 0:
			playerChosen = "Even";
			break;
		case 1:
			playerChosen = "Odd";
			break;
		}
		
		System.out.println(game1.playRound(playerChosen,userMoneyInput));
	}

	

	
/**
 * This method checks if the user correctly guessed if the die is odd or even
 * @param input String (Even or Odd)
 * @param userMoneyInput int (Amount of money)
 * @return boolean
 */
	public String playRound(String input,int userMoneyInput) {
		int myRandom = rand.nextInt(6)+1;
		
		
//checks who won
		if (input.equals("Even") && myRandom%2==0) {
			this.numWins++;
			this.userMoney=this.userMoney+userMoneyInput;
			//adds the amount of the money bet if won
			return "You won " + this.numWins + " times" + " Your input was " + input+ " Your money is" + this.userMoney ;
		} else if ((input.equals("Odd") && myRandom%2==0) || (input.equals("Even") && myRandom%2!=0) ) {
			this.numLosses++;
			this.userMoney=this.userMoney-userMoneyInput;
			return "You lost " + this.numLosses + " times" + " Your input was " + input + " Your money is" + this.userMoney;
		}
		else {
			//The user guessed correctly that the die is odd
			 this.numWins++;
			 this.userMoney=this.userMoney+userMoneyInput;
			 return "You won " + this.numWins + " times" + " Your input was " + input+ " Your money is" + this.userMoney ;
		}
		
	}
	/**
	 * Method checks if the user has input the correct amount of money
	 * @param userMoneyInput int
	 * @return boolean
	 */
	public boolean checkValidMoney (int userMoneyInput){
	if(userMoneyInput>0 && this.userMoney-userMoneyInput>0) {
	// If the user input enough money 
	return true;
	}
	else {
	System.out.println("You need to input more than 0 and an amount of money that you have !");
	return false;
	
	}
	}
}
 

