package DieRoll;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * 
 * @author Maria Barba 1932657
 *
 */
public class DieRollApp extends Application{
	private DieRollGame theGame =new DieRollGame();
	public void start(Stage stage) {
		Group root = new Group(); 

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);
		HBox buttons = new HBox();
		Button button1 = new Button("Even");
		Button button2 = new Button("Odd");
		buttons.getChildren().addAll(button1,button2);
		//Hbox for textfield
		HBox textField = new HBox();
		TextField welcome = new TextField("Amount to bet : ");
		TextField result = new TextField("Your result : ");
		result.setPrefWidth(400);
		textField.getChildren().addAll(welcome,result);
		
		//vbox that has everything

		VBox overall = new VBox();
		overall.getChildren().addAll(buttons,textField);
		
		root.getChildren().add(overall);
		//associate scene to stage and show
		stage.setTitle("Roll Die Game"); 
		stage.setScene(scene); 	
		stage.show(); 
		
		DieRollEvenOrOdd even=new DieRollEvenOrOdd( welcome,result, "Even", theGame);
		button1.setOnAction(even);
		DieRollEvenOrOdd odd=new DieRollEvenOrOdd( welcome,result, "Odd", theGame);
		button1.setOnAction(odd);
		
	}
    public static void main(String[] args) {
        Application.launch(args);
    }
}
